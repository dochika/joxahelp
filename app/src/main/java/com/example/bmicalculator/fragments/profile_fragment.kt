package com.example.bmicalculator.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.bmicalculator.R
import com.example.bmicalculator.loginActivity
import com.google.firebase.auth.FirebaseAuth

class profile_fragment: Fragment(R.layout.activity_profile) {

    private lateinit var button : Button
    private lateinit var button2 : Button
    private lateinit var weight : EditText
    private lateinit var height : EditText
    private lateinit var result : TextView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        button = view.findViewById(R.id.button)
        weight = view.findViewById(R.id.weight)
        height = view.findViewById(R.id.height)
        result = view.findViewById(R.id.result)
        button2 = view.findViewById(R.id.button2)

        val findNavController = Navigation.findNavController(view)

        button.setOnClickListener {




            if (weight.text.toString().isEmpty()){
                return@setOnClickListener
            }
            if (height.text.toString().isEmpty()){
                return@setOnClickListener
            }

            val weight1 : Float = weight.text.toString().toFloat() / 100
            val height1 : Float = height.text.toString().toFloat()
            val bmi : Float = weight1 / (height1 * height1)




            //result.text = bmi.toString()

            val action = profile_fragmentDirections.actionProfileFragment2ToInfofragment(bmi)
            findNavController.navigate(action)
        }


    }
}