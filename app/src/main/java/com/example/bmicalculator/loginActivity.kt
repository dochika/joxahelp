package com.example.bmicalculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import org.w3c.dom.Text

class loginActivity : AppCompatActivity() {

    private lateinit var register1 : TextView
    private lateinit var forgot_password : TextView
    private lateinit var button : Button
    private lateinit var password : EditText
    private lateinit var Email : EditText
    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        register1 = findViewById(R.id.register1)
        forgot_password = findViewById(R.id.forgot_password)
        button = findViewById(R.id.button)
        password = findViewById(R.id.password)
        Email = findViewById(R.id.Email)

        button.setOnClickListener{


            if(Email.text.isNullOrBlank()&&password.text.isNullOrBlank()){
                Toast.makeText(this,"შეიყვანეთ თქვენი მონაცემები",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(Email.text.toString(),password.text.toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this,profileActivity::class.java))
                        finish()
                    }
                }


            if(password.text.toString().isEmpty()){
                password.error = "შეიყვანეთ პაროლი"
                password.requestFocus()
                return@setOnClickListener
            }
            else{
                startActivity(Intent(this,profileActivity::class.java))
                finish()
            }


        }
        register1.setOnClickListener{
            startActivity(Intent(this,signupActivity::class.java))
            finish()
        }

        forgot_password.setOnClickListener{
            startActivity(Intent(this,changepasswordActivity::class.java))
            finish()
        }





    }




}




