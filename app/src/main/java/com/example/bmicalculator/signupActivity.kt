package com.example.bmicalculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import org.w3c.dom.Text
import android.widget.Toast

import com.google.firebase.auth.FirebaseUser

import com.google.firebase.auth.AuthResult

import androidx.annotation.NonNull

import com.google.android.gms.tasks.OnCompleteListener




class signupActivity : AppCompatActivity() {

    private lateinit var auth : FirebaseAuth
    private lateinit var Email : EditText
    private lateinit var password : EditText
    private lateinit var button : Button
    private lateinit var login : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        auth = FirebaseAuth.getInstance()

        Email = findViewById(R.id.Email)
        password = findViewById(R.id.password)
        button = findViewById(R.id.button)
        login = findViewById(R.id.login)

        button.setOnClickListener {
            signUpUser()
            return@setOnClickListener

        }
        login.setOnClickListener{
            startActivity(Intent(this,loginActivity::class.java))
            finish()
        }

    }

    private fun signUpUser()   {

        if(Email.text.toString().isEmpty()){
            Email.error = "შეიყვანეთ ემაილი"
            Email.requestFocus()
        }
        if(Patterns.EMAIL_ADDRESS.matcher(Email.text.toString()).matches()){
            Email.error = "ემაილი გამოყენებულია ან არ არის ნამდვილი"
            Email.requestFocus()
        }
        if(password.text.toString().isEmpty()){
            password.error = "შეიყვანეთ პაროლი"
            password.requestFocus()
        }


        FirebaseAuth.getInstance().createUserWithEmailAndPassword(Email.text.toString() , password.text.toString())
            .addOnCompleteListener{ task ->
                if (task.isSuccessful){
                    startActivity(Intent(this , profileActivity::class.java))
                    finish()
                }else{
                    Toast.makeText(this, "error",Toast.LENGTH_SHORT). show()

                }
            }

    }


}