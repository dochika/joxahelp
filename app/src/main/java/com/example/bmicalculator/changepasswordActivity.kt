package com.example.bmicalculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class changepasswordActivity : AppCompatActivity() {
    private lateinit var Email : EditText
    private lateinit var button : Button
    private lateinit var register : TextView
    private lateinit var login : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_changepassword)

        Email = findViewById(R.id.Email)
        button = findViewById(R.id.button)
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)

        login.setOnClickListener{
            startActivity(Intent(this,loginActivity::class.java))
            finish()
        }
        register.setOnClickListener{
            startActivity(Intent(this,signupActivity::class.java))
            finish()
        }

        button.setOnClickListener {

            if(Email.text.toString().isEmpty()){
                Toast.makeText(this,"შეიყვანეთ ემაილი",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().sendPasswordResetEmail(Email.text.toString())
                .addOnCompleteListener{ task ->
                    if(task.isSuccessful){
                        Toast.makeText(this , "შეამოწმეთ ემაილი" , Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this, "ვერ გაიგზავნა,  სცადეთ თავიდან" , Toast.LENGTH_SHORT).show()
                    }
                }


        }
    }
}